# COBOL tutorial Repo

## Purpose

This repo contains projects and quick sketches used to learn COBOL and GnuCOBOL.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/COBOL.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/cobol/src/master/LICENSE.txt) file for
details.

